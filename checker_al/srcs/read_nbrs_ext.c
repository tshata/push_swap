/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_nbrs_ext.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tshata <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 12:20:13 by tshata            #+#    #+#             */
/*   Updated: 2019/09/02 11:59:45 by tshata           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/checker.h"
#include <stdio.h>

void	read_nbrs_ext(t_stack *s_a, long int nbr, int sign)
{
	nbr *= sign;
	if (!is_valid(s_a, nbr))
	{
		ft_putstr("Error\n");
		exit(0);
	}
	s_a->nbrs[s_a->current_size] = nbr;
	s_a->current_size++;
	nbr = 0;
	sign = 1;
}

int count_nbrs_1(char *str)
{
    int i;
    int nbr;

    i = 0;
    nbr = 0;
    while(str[i])
    {
        if(str[i] == ' ' || str[i] == '\t' || str[i] == '\n')
            i++;
        if((str[i] != ' ' && str[i - 1] == ' '))
           nbr++;
        i++;
    }
    return nbr;
}
void        str_arg(t_stack *s_a, t_stack *s_b, char *argv)
{
    char    **str;
    int     i;
    int     size;

    i = 0;
    size = 0;

    str = ft_strsplit(argv, ' ');
    size = count_nbrs_1(argv);
//	ft_putstr("size :");
//	ft_putnbr(size);
    init(s_a, s_b, size, str);
//	ft_putstr(argv);
    while (i <= size)
    {
    //  ft_putnbr(size);
  //      ft_putchar(*str[i]);

        if(ft_isdigit(ft_atoi(str[i])) && ft_isdigit(ft_atoi(str[i])) != 0)
        {
		ft_putnbr(ft_isdigit(ft_atoi(str[i])));
        read_nbrs(str[i], s_a);
        i++;
        }
		i++;
    }
   // if (is_sorted(s_a->nbrs, size))
     //   exit(1);
}
