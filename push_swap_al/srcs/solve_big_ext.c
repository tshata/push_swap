#include "../includes/push_swap.h"

void	solve_five_1(t_stack *s_a, t_stack *s_b)
{
	if (is_sorted(s_a->nbrs, 5))
	{
		exit(1);
	}
	ft_putendl("pb");
	push_b(s_a, s_b);
	solve_4(s_a, s_b);
	solve_three(s_a);
	ft_putendl("pa");
	ft_putendl("pa");
	push_a(s_a, s_b);
	push_a(s_a, s_b);
}

void	solve_five_2(t_stack *s_a, t_stack *s_b)
{
	ft_putendl("sa");
	swap_top(s_a);
	ft_putendl("pb");
	push_b(s_a, s_b);
	solve_4(s_a, s_b);
	solve_three(s_a);
	ft_putendl("pa");
	ft_putendl("pa");	
	push_a(s_a, s_b);
	push_a(s_a, s_b);

}

void	solve_five_3(t_stack *s_a, t_stack *s_b)
{
	int i = 0;

	ft_putendl("ra");
	rotate(s_a);i = 0;
	ft_putendl("sa");
	swap_top(s_a);
	ft_putendl("pb");
	push_b(s_a, s_b);
	solve_4(s_a, s_b);
	solve_three(s_a);
	ft_putendl("pa");
	ft_putendl("pa");
	push_a(s_a, s_b);
	push_a(s_a, s_b);
}

void	solve_five_4(t_stack *s_a, t_stack *s_b)
{
	ft_putendl("rra");
	reverse_rotate(s_a);
	reverse_rotate(s_a);
	ft_putendl("rra");
	if (is_sorted(s_a->nbrs, 5))
	{
		exit(1);
	}
	push_b(s_a, s_b);
	ft_putendl("pb");
	solve_4(s_a, s_b);
	solve_three(s_a);
	ft_putendl("pa");
	ft_putendl("pa");
	push_a(s_a, s_b);
	push_a(s_a, s_b);
}

void	solve_five_5(t_stack *s_a, t_stack *s_b)
{
	ft_putendl("rra");
	reverse_rotate(s_a);
	if (is_sorted(s_a->nbrs, 5))
	{
		exit(1);
	}
	ft_putendl("pb");
	push_b(s_a, s_b);
	solve_4(s_a, s_b);
	solve_three(s_a);
	ft_putendl("pa");
	ft_putendl("pa");
	push_a(s_a, s_b);
	push_a(s_a, s_b);
}

void ft_display(t_stack *a, t_stack *b)
{	int i = 0;
	while(i < a->max_size)
	{	
		ft_putnbr(a->nbrs[i]);
		ft_putchar('\t');
		ft_putnbr(b->nbrs[i]);
		ft_putchar('\n');
			i++;
	}
}
