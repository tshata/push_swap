/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_nbrs_ext.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tshata <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/17 12:20:13 by tshata            #+#    #+#             */
/*   Updated: 2019/09/03 11:25:45 by tshata           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void		read_nbrs_ext(t_stack *s_a, long int nbr, int sign)
{
	nbr *= sign;
	if (!is_valid(s_a, nbr) && nbr != 0)
	{
		ft_putstr("Error\n");
		exit(0);
	}
	s_a->nbrs[s_a->current_size] = nbr;
	s_a->current_size++;
	nbr = 0;
	sign = 1;
}

int count_nbrs_1(char *str)
{
	int i;
	int nbr;

	i = 0;
	nbr = 0;
	if(str[0])
		nbr++;
	while(str[i])
	{
		if(str[i] == ' '|| str[i] == '\t' || str[i] == '\n')
			i++;
		if((!(str[i] == ' ') && (str[i - 1] == ' ')) && (str[0])) 		
		   nbr++;
		i++;	
	}
	return nbr;
}

void		str_arg(t_stack *a, t_stack *b, char *argv)
{
	char	**str;
	int		i;
	int		size;
	
	i = 0;
	size = 0;
	str = ft_strsplit(argv, ' ');
	size += count_nbrs_1(argv);
	init(a, b, size, str);
	while (i < size)
	{
		if(!ft_isdigit(ft_atoi(str[i])))
		{
			read_nbrs(str[i], a);
			i++;

		}
	}
	choose_solution(*a, *b, size);
}
