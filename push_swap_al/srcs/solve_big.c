/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_big.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tshata <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 07:21:31 by tshata            #+#    #+#             */
/*   Updated: 2019/11/30 14:23:06 by tshata           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"
static void ft_rotations(t_stack *s_a, t_stack *s_b)
{}


void	ft_sort_big(t_stack *a, t_stack *b)
{
	find_max(b);
	int count = 0;
	while(count < b->max_nbr)
	{	
		find_min(b);
		if(b->nbrs[0] != b->max_nbr)
		if(b->nbrs[0] < b->nbrs[1])
		{
			ft_putendl("sb");
			swap_top(b);	
		}
		find_max(b);
		while(b->nbrs[0] == b->max_nbr)
		{		
			ft_putendl("pa");	
			push_a(a, b);
			b->nbrs[b->max_size - 1] = 0;
			b->max_size--;
		}	
		ft_putendl("rb");	
		rotate(b);
	}

}
void	ft_sort_big_1(t_stack *a, t_stack *b)
{
		if(b->nbrs[0] < b->nbrs[1])
		{
			ft_putendl("sb");
			swap_top(b);
		}
}

void	solve_big(t_stack *a, t_stack *b)
{
	while(!is_sorted(a->nbrs, a->max_size))
	{
		ft_rotations(a, b);
		ft_putendl("pb");
			push_b(a, b);
		if(a->nbrs[0] > a->nbrs[1])
		{
			ft_putendl("pb");
			push_b(a, b);
			ft_sort_big_1(a ,b);
			ft_swaps(a, b);
		}
		if(a->nbrs[0] < a->nbrs[1])
		{
			swap_top(a);
			write(1, "sa\n", 3);
			ft_putendl("pb");
			push_b(a, b);
			ft_sort_big_1(a ,b);
		}
		ft_sort_big(a,b);
		a->nbrs[a->current_size] = 0;
	}
	ft_sort_big(a, b);
}

void ft_swaps(t_stack *a, t_stack *b)
{
	if(a->nbrs[0] > a->nbrs[1] && b->nbrs[0] < b->nbrs[1])
	{
		ft_putendl("ss");
		swap_both(a, b);
	}
	if(b->nbrs[0] < b->nbrs[1])
	{
		swap_top(b);
		write(1, "sb\n", 3);
	}
	if(a->nbrs[0] > a->nbrs[1])
	{
		swap_top(a);
		write(1, "sa\n", 3);
	}		
}
