/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate_functs.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tshata <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/01 14:37:59 by tshata            #+#    #+#             */
/*   Updated: 2019/02/15 07:55:14 by tshata           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	rotate(t_stack *s)
{
	int	temp;

	temp = (s->nbrs)[0];
	ft_memmove(&(s->nbrs[0]), &(s->nbrs[1]), sizeof(int) * (s->current_size));
	s->nbrs[s->current_size - 1] = temp;
	temp = 0;
}

void	rotate_both(t_stack *s_a, t_stack *s_b)
{
	rotate(s_a);
	rotate(s_b);
}

void	reverse_rotate(t_stack *s)
{
	int	temp;
	
	temp = s->nbrs[s->current_size - 1];
	ft_memmove(&(s->nbrs[1]), &(s->nbrs[0]), sizeof(int) * (s->current_size));
	s->nbrs[0] = temp;
	temp = 0;
}

void	reverse_rotate_both(t_stack *s_a, t_stack *s_b)
{
	reverse_rotate(s_a);
	reverse_rotate(s_b);
}
